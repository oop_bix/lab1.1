import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {

        Scanner scanner = new Scanner(System.in);
        xoGame game = new xoGame();
        int col;
        int row;
        int round = 1;

        System.out.println("Welcome TO Xo game!");
        game.display();
        while (true){
            if (round%2==0){
                game.setPlayer('o');
            }else{game.setPlayer('x');}

            game.setPlayer(game.getPlayer());
            System.out.printf("Now %s Turn Plesase input(row,col): ",game.getPlayer());
            row = scanner.nextInt();
            col = scanner.nextInt();
            game.setCol(col);
            game.setRow(row);
            game.fill();
            game.display();
            if(game.checkWin()==true){
                System.out.printf("%s Win!!!",game.getPlayer());
                break;
            }
            else if(round == 9 && (game.checkWin()==false)){
                System.out.println(" Draw Draw Draw Draw!");
                break;
            }
            
            round++;
        }
        
        scanner.close();
        

    }
}
